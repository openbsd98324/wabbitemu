# wabbitemu

wabbitemu

Wabbitemu creates a Texas Instruments graphing calculator right on your Windows, Mac, or Android device. Wabbitemu supports the TI-73, TI-81, TI-82, TI-83, TI-83 Plus, TI-83 Plus Silver Edition, TI-84 Plus, TI-84 Plus Silver Edition, TI-85, and TI-86. Fast and convenient, Wabbitemu allows you to always have your trusty calculator with you. Because Wabbitemu is an emulator, the calculator it creates will act exactly like the real thing. 


## Ti-84 PC SE (colors)


 TI-84 Plus C SE rom that can be used for instance with Wabbitemu (under Windows, Linux,...). 

Calculator ti84pcse:


![](medias/ti84pcse-1.png)

![](medias/ti84pcse-2.png)



![](medias/ti84pcse-3-detached-lcd.png)

![](medias/ti84pcse-4-frac-detached-lcd.png)


## Graphics 

![](medias/calc-log-ti84pcse-vs-ti84plus-1.png)


## Linux

4.9 i386/686 kernel using Linux Devuan.

![](medias/1653212846-1-wabbitemu-w1.png)

![](medias/1653212846-2-wabbitemu-w2.png)



